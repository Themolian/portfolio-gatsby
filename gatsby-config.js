/**
 * @type {import('gatsby').GatsbyConfig}
 */
require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
});

module.exports = {
  siteMetadata: {
    title: `Jamie Curran - Portfolio`,
    siteUrl: `https://www.jamiecurran.tech`,
  },
  plugins: [
    "gatsby-plugin-sass",
    "gatsby-plugin-image",
    "gatsby-plugin-mdx",
    "gatsby-plugin-sharp",
    "gatsby-transformer-sharp",
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "images",
        path: "./src/images/",
      },
      __key: "images",
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "pages",
        path: "./src/pages/",
      },
      __key: "pages",
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "blog",
        path: "./src/pages/blog",
      },
      __key: "blog",
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "projects",
        path: "./src/pages/projects",
      },
      __key: "projects",
    },
    {
      resolve: `gatsby-source-wordpress`,
      options: {
        url: "https://cms.jamiecurran.tech/graphql",
      },
      schema: {
        typePrefix: `Wp`,
      },
      develop: {
        hardCacheMediaFiles: true,
      },
      // type: {
      //   Post: {
      //     limit: {
      //       process.env.NODE_ENV === `development`
      //     }
      //   }
      // }
    },
  ],
};
